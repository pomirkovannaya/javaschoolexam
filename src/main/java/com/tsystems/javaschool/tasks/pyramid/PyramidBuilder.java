package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */


    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        if (Objects.isNull(inputNumbers) || inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException("Incorrect input. Input list is empty or null.");
        }

        int countNumbers = inputNumbers.size();
        double n = (Math.sqrt(1 + 8 * countNumbers) - 1) / 2;
        if (n % 1 != 0) {
            throw new CannotBuildPyramidException("The wrong count of numbers to build the pyramid.");
        }
        int countRows = (int) n;
        int rowLength = 2 * countRows - 1;

        for (Integer number : inputNumbers) {
            if (Objects.isNull(number)) {
                throw new CannotBuildPyramidException("Incorrect input. Some of the number is null.");
            }
        }
        Collections.sort(inputNumbers);

        int[][] pyramid = new int[countRows][rowLength];
        ListIterator<Integer> iterator = inputNumbers.listIterator();
        for (int i = 1; i <= countRows; i++) {
            for (int j = countRows - i; j <= rowLength - countRows + i; j = j + 2) {
                pyramid[i - 1][j] = iterator.next();
                if (!iterator.hasNext()) break;
            }
        }
        return pyramid;
    }
}
