package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")


    public boolean find(List x, List y) throws IllegalArgumentException {
        if (Objects.isNull(x) || Objects.isNull(y)) {
            throw new IllegalArgumentException("Incorrect input. Some of lists is null.");
        }

        if (x.isEmpty()) return true;
        if (y.isEmpty()) return false;

        int i = 0;
        for (Object yElement : y) {
            if (Objects.isNull(x.get(i)) || Objects.isNull(yElement)) {
                throw new IllegalArgumentException("Incorrect input. Some element of a list is null.");
            }
            if (yElement.equals(x.get(i))) i++;
            if (i == x.size()) return true;
        }
        return false;
    }
}


