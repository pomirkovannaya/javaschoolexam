package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class Calculator {
    private final Map<Character, Integer> operationPriority = new HashMap<>();

    public Calculator() {
        operationPriority.put('(', 0);
        operationPriority.put(')', 4);
        operationPriority.put('+', 1);
        operationPriority.put('-', 1);
        operationPriority.put('*', 2);
        operationPriority.put('/', 2);
        operationPriority.put('~', 3);
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (Objects.isNull(statement) || statement.isEmpty()) return null;

        List<String> postfixExpr = convertToPostfixNotation(statement);
        if (postfixExpr.isEmpty() || postfixExpr.contains("(") || postfixExpr.contains(")")) return null;

        Stack<Double> stack = new Stack<>();
        for (String string : postfixExpr) {
            if (isNumeric(string)) {
                stack.push(Double.parseDouble(string));
            } else if (string.equals("~")) {
                stack.push(0 - stack.pop());
            } else {
                double tmp1 = stack.pop();
                double tmp2 = stack.pop();

                switch (string) {
                    case "*":
                        stack.push(tmp2 * tmp1);
                        break;
                    case "/":
                        if (tmp2 / tmp1 == Double.NEGATIVE_INFINITY || tmp2 / tmp1 == Double.POSITIVE_INFINITY) {  // Division by 0
                            return null;
                        }
                        stack.push(tmp2 / tmp1);
                        break;
                    case "+":
                        stack.push(tmp2 + tmp1);
                        break;
                    case "-":
                        stack.push(tmp2 - tmp1);
                        break;
                }
            }
        }

        if (!stack.empty()) {
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#.####", otherSymbols);
            return decimalFormat.format(stack.pop());
        } else {
            return null;
        }
    }

    private List<String> convertToPostfixNotation(String statement) {
        List<String> postfixExpr = new ArrayList<>();
        Stack<Character> stack = new Stack<>();
        char[] input = statement.toCharArray();
        int idx = 0;

        while (idx < input.length) {
            if (Character.isDigit(input[idx])) {
                String s = input[idx++] + "";
                while (idx < input.length && !operationPriority.containsKey(input[idx])) {
                    s = s.concat(input[idx++] + "");
                }
                if (isNumeric(s)) {
                    postfixExpr.add(s);
                } else return new ArrayList<>();
            } else if (input[idx] == '(') {
                stack.push(input[idx++]);
            } else if (input[idx] == ')') {
                if (stack.isEmpty()) return new ArrayList<>();
                while (!stack.isEmpty() && stack.peek() != '(') {
                    postfixExpr.add(stack.pop().toString());
                }
                stack.pop();
                idx++;
            } else if (isArithmeticOperation(input[idx])) {
                if (idx + 1 == input.length || isArithmeticOperation(input[idx + 1])) return new ArrayList<>();
                if ((input[idx] == '-') && (idx == 0 || input[idx - 1] == '(')) {
                    stack.push('~');
                    idx++;
                } else {
                    while (stack.size() > 0 &&
                            operationPriority.get(stack.peek()) >= operationPriority.get(input[idx])) {
                        postfixExpr.add(stack.pop().toString());
                    }
                    stack.push(input[idx++]);
                }
            } else return new ArrayList<>();
        }

        while (!stack.isEmpty()) {
            postfixExpr.add(stack.pop().toString());
        }

        return postfixExpr;
    }

    private static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean isArithmeticOperation(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }
}
